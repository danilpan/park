FROM golang:1.18.1-buster as build

WORKDIR /app
COPY . .

RUN go mod download
RUN go mod verify

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin ./cmd

FROM scratch

COPY --from=build /app/conf/config.json /conf/config.json
COPY --from=build /app/bin /bin

CMD ["/bin"]