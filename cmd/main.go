package main

import (
	"context"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"os"
	"os/signal"
	"processor/internal/transport/http"
	"syscall"
)

// @title client
// @version 1.0
// @description Карточка клиента
// @termsOfService http://swagger.io/terms/
// @scheme http
// @host
// @BasePath
func main() {
	log.Fatalln(fmt.Sprintf("Service shut down: %s", run()))
}

func run() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	errCh := make(chan error, 1)

	go http.StartServer(ctx, errCh)

	go gracefulShutdown(errCh)

	return <-errCh
}

func gracefulShutdown(errCh chan<- error) {
	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)
	errCh <- fmt.Errorf("%s", <-sigCh)
}
