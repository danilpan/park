package service

import (
	"gitlab.com/qlife2/logger"

	//nolint:gosec
	"net"
	"net/http"
	"net/url"
	"processor/internal/config"
	"time"
)

type ProcessorService struct {
	cfg         *config.Config
	client      *http.Client
	logger      *logger.Logger
	proxyClient *http.Client
}

func NewProcessorService(cfg *config.Config, client *http.Client, logger *logger.Logger) *ProcessorService {
	proxyUrl, _ := url.Parse(cfg.Server.Proxy)
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:    100,
		IdleConnTimeout: 90 * time.Second,
		Proxy:           http.ProxyURL(proxyUrl),
	}
	proxyClient := &http.Client{
		Transport: transport,
	}
	return &ProcessorService{cfg: cfg, client: client, logger: logger, proxyClient: proxyClient}
}
