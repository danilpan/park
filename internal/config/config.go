package config

type Config struct {
	Server   *ServerConfig   `json:"server"`
	Metric   *MetricConfig   `json:"metric"`
	Database *DatabaseConfig `json:"database"`
	CORS     *CORS           `json:"cors"`
	Metadata *Metadata       `json:"metadata"`
	Auth     *Auth           `json:"auth"`
	Tracer   *TracerConfig   `json:"tracer"`
}

func NewConfig(name, version string) *Config {
	return &Config{
		Server: &ServerConfig{
			Name:    name,
			Version: version,
		},
		Metric:   &MetricConfig{},
		Database: &DatabaseConfig{},
		CORS:     &CORS{},
		Metadata: &Metadata{},
		Tracer:   &TracerConfig{},
	}
}

type ServerConfig struct {
	Addr    string `json:"addr" env:"SERVER_ADDR"`
	Name    string `json:"name" env:"SERVER_NAME"`
	Version string `json:"version" env:"SERVER_VERSION"`
	Proxy   string `json:"proxy"`
}

type MetricConfig struct {
	Addr string `json:"addr" env:"METRIC_ADDR"`
}

type DatabaseConfig struct {
	Primary     string `json:"primary"`
	Standby     string `json:"standby"`
	Test        string `json:"test"`
	PrimaryAddr string `json:"primary_addr"`
	StandbyAddr string `json:"standby_addr"`
	TestAddr    string `json:"test_addr"`
}

type CORS struct {
	AllowOrigins     string `json:"allow_origins"`
	AllowHeaders     string `json:"allow_headers"`
	AllowMethods     string `json:"allow_methods"`
	AllowCredentials string `json:"allow_credentials"`
	ExposeHeaders    string `json:"expose_headers"`
	MaxAge           string `json:"max_age"`
}

type TracerConfig struct {
	Host string `env:"JAEGER_AGENT_HOST" json:"host" default:"127.0.0.1"`
	Port string `env:"JAEGER_AGENT_PORT" json:"port" default:"6831"`
}

type Metadata struct {
	OutgoingSize int `json:"outgoing_size"`
}

type Auth struct {
	Enable bool `json:"enable"`
}
