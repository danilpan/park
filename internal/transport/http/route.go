package http

import (
	"context"
	"github.com/labstack/echo/v4"
	echoMw "github.com/labstack/echo/v4/middleware"
	"gitlab.com/qlife2/logger"
	"net"
	"net/http"
	"processor/internal/config"
	"processor/internal/service"
	"processor/internal/transport/http/handler"
	"time"
)

func Init(cfg *config.Config, ctx context.Context, logger *logger.Logger) *echo.Echo {
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:    100,
		IdleConnTimeout: 90 * time.Second,
	}
	client := &http.Client{
		Transport: transport,
	}
	defer client.CloseIdleConnections()

	ps := service.NewProcessorService(cfg, client, logger)
	dh := handler.NewDatabaseHandler(cfg, logger, ps)

	e := echo.New()
	e.Debug = true
	// Set Bundle MiddleWare
	e.Use(echoMw.Gzip())
	e.Use(echoMw.CORSWithConfig(echoMw.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"*"},
	}))

	// Routes
	e.GET("/live", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})
	e.GET("/ready", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})

	excludeUrls := make(map[string]interface{})
	excludeUrls["/ready"] = nil
	excludeUrls["/live"] = nil
	excludeUrls["/metrics"] = nil

	v2 := e.Group("/api/v1")
	//Поиск клиента по ИИН
	v2.GET("/check", dh.HealthCheck)
	return e
}
