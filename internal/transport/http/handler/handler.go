package handler

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/qlife2/logger"
	"processor/internal/config"
	"processor/internal/service"
)

type DatabaseHandler struct {
	cfg     *config.Config
	cLogger *logger.Logger
	ps      *service.ProcessorService
}

func NewDatabaseHandler(config *config.Config, logger *logger.Logger, ps *service.ProcessorService) *DatabaseHandler {
	return &DatabaseHandler{
		cfg:     config,
		cLogger: logger,
		ps:      ps,
	}
}

func (dh DatabaseHandler) HealthCheck(c echo.Context) error {
	return c.String(200, "Hello")
}
