package http

import (
	"context"
	"encoding/json"
	"github.com/labstack/echo-contrib/jaegertracing"
	"github.com/labstack/echo-contrib/prometheus"
	"gitlab.com/qlife2/logger"
	"gitlab.com/qlife2/logger/middleware"
	"io"
	"log"
	"os"
	"processor/internal/config"
)

func readConfigFile(directory, filename string) ([]byte, error) {
	cfgFile, fileErr := os.Open(directory + "/" + filename)

	if fileErr != nil {
		return nil, fileErr
	}
	return io.ReadAll(cfgFile)
}

func prepareConfigStruct(common []byte) (*config.Config, error) {
	cfg := config.NewConfig("processor", os.Getenv("SERVER_VERSION"))

	if unmFileErr := json.Unmarshal(common, cfg); unmFileErr != nil {
		return nil, unmFileErr
	}
	return cfg, nil
}

func StartServer(ctx context.Context, errCh chan error) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	//prefixPath := os.Getenv("CONFIG_PATH")
	prefixPath := "conf"
	commonCfg, commonCfgErr := readConfigFile(prefixPath, "config.json")

	if commonCfgErr != nil {
		errCh <- commonCfgErr
		return
	}
	cfg, cfgErr := prepareConfigStruct(commonCfg)

	if cfgErr != nil {
		errCh <- cfgErr
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cLogger := logger.NewConsoleLogger(logger.INFO, logger.JSON)

	excludeUrls := make(map[string]interface{})
	excludeUrls["/ready"] = nil
	excludeUrls["/live"] = nil
	excludeUrls["/metrics"] = nil

	lm := middleware.NewLoggerMiddleware(cLogger, middleware.WithExcludeUrls(excludeUrls))

	app := Init(cfg, ctx, cLogger)
	app.Use(lm.HandleEchoLogger)

	p := prometheus.NewPrometheus("client", nil)
	p.Use(app)
	c := jaegertracing.New(app, nil)

	cLogger.InfoCtx(ctx, "Houston, flight is normal")

	defer func(closer io.Closer) {
		if err := c.Close(); err != nil {
			cLogger.Warn("Cannot close resources")
		}
	}(c)
	errCh <- app.Start(cfg.Server.Addr)
}
